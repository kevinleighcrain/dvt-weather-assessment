package com.dvt.weather.data.local

import com.dvt.weather.module.DatabaseModule
import com.dvt.weather.test.BaseUnitTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import javax.inject.Inject

@HiltAndroidTest
@UninstallModules(DatabaseModule::class)
class WeatherDaoTest : BaseUnitTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var database: WeatherDatabase
    @Inject
    lateinit var weatherDao: WeatherDao

    @Before
    @Throws(IOException::class)
    fun tearDown() {
        hiltRule.inject()
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun insert_cannotInsertDuplicateCity() = runBlocking {
        withContext(Dispatchers.Main) {
            val cityId = 1L

            weatherDao.insert(newWeatherLocation(cityId))
            weatherDao.insert(newWeatherLocation(cityId))

            val weatherLocations = weatherDao.getAll().first()
            assertThat(weatherLocations).isNotEmpty()
            assertThat(weatherLocations).hasSize(1)
        }
    }

    @Test
    fun findByCityId_returnsCorrectWeatherLocation() = runBlocking {
        withContext(Dispatchers.Main) {
            val cityId = 1L

            weatherDao.insert(newWeatherLocation(cityId))

            val weatherLocation = weatherDao.findByCityId(cityId)
            assertThat(weatherLocation).isNotNull()
            assertThat(weatherLocation?.cityId).isEqualTo(cityId)
        }
    }

    @Test
    fun updateCurrentWeather_makesUpdateCorrectly() = runBlocking {
        withContext(Dispatchers.Main) {
            val cityId = 1L
            val currentWeather = "dummy_currentWeather"
            weatherDao.insert(newWeatherLocation(cityId))

            weatherDao.updateCurrentWeather(
                WeatherCurrentWeatherUpdate(
                    id = 1L,
                    currentWeather = currentWeather
                )
            )

            val weatherLocation = weatherDao.findByCityId(cityId)
            assertThat(weatherLocation).isNotNull()
            assertThat(weatherLocation?.currentWeather).isEqualTo(currentWeather)
        }
    }

    @Test
    fun updateWeatherForecast_makesUpdateCorrectly() = runBlocking {
        withContext(Dispatchers.Main) {
            val cityId = 1L
            val weatherForecast = "dummy_weatherForecast"
            weatherDao.insert(newWeatherLocation(cityId))

            weatherDao.updateWeatherForecast(
                WeatherForecastUpdate(
                    id = 1L,
                    weatherForecast = weatherForecast
                )
            )

            val weatherLocation = weatherDao.findByCityId(cityId)
            assertThat(weatherLocation).isNotNull()
            assertThat(weatherLocation?.weatherForecast).isEqualTo(weatherForecast)
        }
    }

}

fun newWeatherLocation(cityId: Long) = Weather(
    latitude = 0.0,
    longitude = 0.0,
    city = "",
    cityId = cityId
)