package com.dvt.weather.data.selectors

import com.dvt.weather.R
import com.dvt.weather.data.remote.WeatherForecastResponse
import com.dvt.weather.test.BaseUnitTest
import com.dvt.weather.test.rawResJsonToObject
import org.junit.Assert.assertEquals
import org.junit.Test

class WeatherForecastSelectorTest : BaseUnitTest() {

    @Test
    fun forDayAndHour_returnsCorrectWeatherForDay() {
        val response =
            context.rawResJsonToObject(R.raw.weather_forecast, WeatherForecastResponse::class.java)
        val weatherForecast = WeatherForecastSelector(response)

        val weatherForDay = weatherForecast.forDayAndHour(14, 15)

        assertEquals("Rain", CurrentWeatherSelector(weatherForDay).conditionName())
    }
}
