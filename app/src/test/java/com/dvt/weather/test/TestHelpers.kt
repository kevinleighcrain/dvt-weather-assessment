package com.dvt.weather.test

import android.content.Context
import androidx.annotation.RawRes
import com.google.gson.Gson

fun Context.rawResText(@RawRes id: Int): String {
    val stream = resources.openRawResource(id)
    return stream.bufferedReader(Charsets.UTF_8).readText()
}

fun <T> Context.rawResJsonToObject(@RawRes id: Int, cls: Class<T>): T {
    return Gson().fromJson(rawResText(id), cls)
}
