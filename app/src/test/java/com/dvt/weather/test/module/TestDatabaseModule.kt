package com.dvt.weather.test.module

import android.content.Context
import androidx.room.Room
import com.dvt.weather.data.local.WeatherDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class TestDatabaseModule {

    @Singleton
    @Provides
    fun provideWeatherDatabase(@ApplicationContext context: Context) =
        Room.inMemoryDatabaseBuilder(
            context, WeatherDatabase::class.java
        ).build()

    @Singleton
    @Provides
    fun provideWeatherLocationDao(database: WeatherDatabase) = database.weatherDao()

    @Singleton
    @Provides
    fun provideSavedLocationDao(database: WeatherDatabase) = database.savedLocationDao()

}