package com.dvt.weather.test

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import dagger.hilt.android.testing.HiltTestApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28], application = HiltTestApplication::class)
abstract class BaseUnitTest {

    val context: Context by lazy {
        ApplicationProvider.getApplicationContext<Context>()
    }

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUpCoroutineDispatcher() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    @Throws(IOException::class)
    fun tearDownCoroutineDispatcher() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

}
