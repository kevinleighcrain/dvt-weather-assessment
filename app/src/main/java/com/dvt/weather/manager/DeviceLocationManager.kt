package com.dvt.weather.manager

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationCallback
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Manager class used to assist in retrieving device location
 */
class DeviceLocationManager @Inject constructor(
    @ApplicationContext private val context: Context
) : PermissionListener {

    /**
     * Internal client used to provide device location
     *
     * @see FusedLocationProviderClient
     */
    private var internalClient: FusedLocationProviderClient? = null

    /**
     * Live data for consuming location result
     *
     * @see Location
     */
    val locationLiveData = MutableLiveData<Location>()

    /**
     * Live data for consuming location error codes
     *
     * @see ERROR_LOCATION_NULL
     * @see ERROR_PERMISSION_PERMANENTLY_DENIED
     */
    val locationErrorLiveData = MutableLiveData<Int>()

    /**
     * Fetches device location
     *
     * When User has not accepted location runtime permissions, then method will prompt User to accept
     * location permission before attempting to fetch device current location.
     *
     * @see Dexter.check
     * @see Manifest.permission.ACCESS_FINE_LOCATION
     */
    fun fetch() {
        Log.d(javaClass.simpleName, "Attempt fetching current location...")

        Dexter.withContext(context)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(this)
            .onSameThread()
            .check()
    }

    /**
     * Requests device location
     */
    @SuppressLint("MissingPermission")
    private fun requestCurrentLocation() {
        Log.d(javaClass.simpleName, "Fetching current location...")

        val locationRequest = with(LocationRequest.create()) {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            this
        }
        with(LocationServices.getFusedLocationProviderClient(context)) {
            requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
            internalClient = this
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
            super.onLocationResult(result)
            internalClient?.removeLocationUpdates(this)

            val lastLocation = result?.lastLocation
            if (lastLocation != null) {
                locationLiveData.postValue(lastLocation)
            } else {
                locationErrorLiveData.postValue(ERROR_LOCATION_NULL)
            }
        }
    }

    /**
     * Invoked when location permission is granted
     *
     * Method will proceed to request device location
     */
    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
        requestCurrentLocation()
    }

    /**
     * Invoked when location permission is denied
     *
     * Method will check if location permission is permanently denied, if true, then method will
     * post subsequent error code
     *
     * @param response Permission denied response
     *
     * @see ERROR_PERMISSION_PERMANENTLY_DENIED
     */
    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
        if (response?.isPermanentlyDenied == true) {
            locationErrorLiveData.postValue(ERROR_PERMISSION_PERMANENTLY_DENIED)
        } else {
            locationErrorLiveData.postValue(ERROR_PERMISSION_DENIED)
        }
    }

    /**
     * Invoked when app is prompted to show permission rationale
     *
     * Method will check proceed to continue permission request
     *
     * @param request Permission request
     * @param token Permission token
     */
    override fun onPermissionRationaleShouldBeShown(
        request: PermissionRequest?,
        token: PermissionToken?
    ) {
        token?.continuePermissionRequest()
    }

    companion object {
        /**
         * Error code for when location result is null
         */
        const val ERROR_LOCATION_NULL = 0

        /**
         * Error code for when location permission is denied
         */
        const val ERROR_PERMISSION_DENIED = 1

        /**
         * Error code for when location permission is permanently denied
         */
        const val ERROR_PERMISSION_PERMANENTLY_DENIED = 2
    }

}