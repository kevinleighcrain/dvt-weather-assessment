package com.dvt.weather

import android.app.Application
import com.google.android.libraries.places.api.Places
import dagger.hilt.android.HiltAndroidApp

/**
 * DVT Weather application
 */
@HiltAndroidApp
class DVTWeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initializes places api
        Places.initialize(this, BuildConfig.GC_API_KEY)
    }

}