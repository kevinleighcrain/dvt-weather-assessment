package com.dvt.weather.module

import com.dvt.weather.BuildConfig
import com.dvt.weather.data.remote.ForecastApi
import com.dvt.weather.data.remote.WeatherApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Module for providing API components for DVT Weather App
 */
@InstallIn(ApplicationComponent::class)
@Module
class ApiModule {

    /**
     * Provides OkHttp client used for DVT Weather App api
     *
     * @return OkHttpClient
     */
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = with(OkHttpClient.Builder()) {
        addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
        val timeOut = BuildConfig.API_REQUEST_TIMEOUT.toLong()
        val timeUnit = TimeUnit.SECONDS
        connectTimeout(timeOut, timeUnit)
        readTimeout(timeOut, timeUnit)
        writeTimeout(timeOut, timeUnit)
        build()
    }

    /**
     * Provides converter factory object used to serialize/deserialize weather api
     * requests and responses
     *
     * @return OkHttpClient
     */
    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory = GsonConverterFactory.create()

    /**
     * Provides Retrofit client for creating implementations of weather api
     *
     * @return Retrofit
     */
    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, converterFactory: Converter.Factory): Retrofit =
        with(Retrofit.Builder()) {
            baseUrl(BuildConfig.OPEN_WEATHER_API_URL)
            client(okHttpClient)
            addConverterFactory(converterFactory)
            build()
        }

    /**
     * Provides Weather api
     *
     * @return WeatherApi
     */
    @Provides
    @Singleton
    fun provideWeatherApi(retrofit: Retrofit): WeatherApi =
        retrofit.create(WeatherApi::class.java)

    /**
     * Provides Weather forecast api
     *
     * @return ForecastApi
     */
    @Provides
    @Singleton
    fun provideForecastApi(retrofit: Retrofit): ForecastApi =
        retrofit.create(ForecastApi::class.java)

}