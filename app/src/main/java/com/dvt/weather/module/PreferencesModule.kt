package com.dvt.weather.module

import com.dvt.weather.data.local.Preferences
import com.dvt.weather.data.local.PreferencesImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

/**
 * Module for providing app preferences for DVT Weather App
 */
@Module
@InstallIn(ActivityComponent::class)
abstract class PreferencesModule {

    /**
     * Provides preferences implementation
     *
     * @return Preferences
     */
    @Binds
    abstract fun bindPreferences(
        preferencesImpl: PreferencesImpl
    ): Preferences

}