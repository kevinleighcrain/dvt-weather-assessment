package com.dvt.weather.module

import android.content.Context
import androidx.room.Room
import com.dvt.weather.data.local.WeatherDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

/**
 * Module for providing Database components for DVT Weather App
 */
@InstallIn(ApplicationComponent::class)
@Module
class DatabaseModule {

    /**
     * Provides weather database
     *
     * @return WeatherDatabase
     */
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): WeatherDatabase {
        return Room.databaseBuilder(
            context,
            WeatherDatabase::class.java,
            "dvt.weather.db"
        ).build()
    }

    /**
     * Provides weather dao
     *
     * @return WeatherDao
     */
    @Provides
    @Singleton
    fun provideWeatherDao(database: WeatherDatabase) = database.weatherDao()

    /**
     * Provides saved location dao
     *
     * @return SavedLocationDao
     */
    @Provides
    @Singleton
    fun provideSavedLocation(database: WeatherDatabase) = database.savedLocationDao()

}