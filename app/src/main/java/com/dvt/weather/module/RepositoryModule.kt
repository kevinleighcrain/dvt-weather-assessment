package com.dvt.weather.module

import com.dvt.weather.data.repository.ForecastRepository
import com.dvt.weather.data.repository.ForecastRepositoryImpl
import com.dvt.weather.data.repository.WeatherRepository
import com.dvt.weather.data.repository.WeatherRepositoryImpl
import com.dvt.weather.data.repository.WeatherDatabaseRepository
import com.dvt.weather.data.repository.WeatherDatabaseRepositoryImpl
import com.dvt.weather.data.repository.SavedLocationsRepository
import com.dvt.weather.data.repository.SavedLocationsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent


/**
 * Module for repository implementations for DVT Weather App
 */
@Module
@InstallIn(ActivityComponent::class)
abstract class RepositoryModule {

    /**
     * Provides forecast repository
     *
     * @return ForecastRepository
     */
    @Binds
    abstract fun bindForecastRepository(
        forecastRepositoryImpl: ForecastRepositoryImpl
    ): ForecastRepository

    /**
     * Provides saved locations repository
     *
     * @return SavedLocationsRepository
     */
    @Binds
    abstract fun bindSavedLocationsRepository(
        savedLocationsRepositoryImpl: SavedLocationsRepositoryImpl
    ): SavedLocationsRepository

    /**
     * Provides weather database repository
     *
     * @return WeatherDatabaseRepository
     */
    @Binds
    abstract fun bindWeatherDatabaseRepository(
        weatherDatabaseRepositoryImpl: WeatherDatabaseRepositoryImpl
    ): WeatherDatabaseRepository

    /**
     * Provides weather repository
     *
     * @return WeatherRepository
     */
    @Binds
    abstract fun bindWeatherRepository(
        weatherRepository: WeatherRepositoryImpl
    ): WeatherRepository
}
