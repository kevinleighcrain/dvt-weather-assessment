package com.dvt.weather.helpers

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode

/**
 * Helper object to assist with handling places auto complete api functionalitu=y
 *
 * See https://developers.google.com/places/android-sdk/autocomplete
 */
object PlacesAutoCompleteHelper {

    /**
     * Request code used when starting autocomplete intent
     *
     * @see Activity.startActivityForResult
     * @see Fragment.startActivityForResult
     */

    private const val RC_AUTOCOMPLETE = 1

    /**
     * Show places autocomplete activity overlay
     *
     * @see Autocomplete.IntentBuilder
     * @see AutocompleteActivityMode.OVERLAY
     */
    fun showAutoComplete(fragment: Fragment) {
        val fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS,
            Place.Field.PHONE_NUMBER,
            Place.Field.WEBSITE_URI,
            Place.Field.RATING
        )
        with(Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)) {
            val intent = build(fragment.requireContext())
            fragment.startActivityForResult(intent, RC_AUTOCOMPLETE)
        }
    }

    /**
     * Receives places object from activity result
     *
     * @param requestCode Request code param passed to activity/fragment result
     * @param resultCode Result code param passed to activity/fragment result
     * @param data Intent param passed to activity/fragment result
     * @param onPlace Callback that is invoked when place is available
     * @param onPlaceError Callback that is invoked when there is an error receiving place
     *
     * @see Activity.onActivityResult
     * @see Fragment.onActivityResult
     */
    fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
        onPlace: (Place) -> Unit,
        onPlaceError: (Status) -> Unit
    ) {
        if (requestCode != RC_AUTOCOMPLETE) {
            return
        }
        when (resultCode) {
            Activity.RESULT_OK -> {
                data?.let {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    onPlace(place)
                    Log.d(javaClass.simpleName, "Place: ${place.name}, ${place.id}")
                }
            }
            AutocompleteActivity.RESULT_ERROR -> {
                data?.let {
                    val status = Autocomplete.getStatusFromIntent(data)
                    onPlaceError(status)
                    Log.d(javaClass.simpleName, "${status.statusMessage}")
                }
            }
        }
    }
}
