package com.dvt.weather.data.repository

import com.dvt.weather.data.local.Preferences
import com.dvt.weather.data.remote.ForecastApi
import com.dvt.weather.data.selectors.WeatherForecastSelector
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Interface for weather forecast repository
 */
interface ForecastRepository {

    /**
     * Gets the weather forecast for location from api
     */
    suspend fun getWeatherForecast(
        latitude: Double,
        longitude: Double
    ): Result<WeatherForecastSelector>

    /**
     * Gets weather forecast last received by app when network is offline
     */
    suspend fun getOfflineWeatherForecast(): Result<WeatherForecastSelector>

}

class ForecastRepositoryImpl @Inject constructor(
    private val forecastApi: ForecastApi,
    private val weatherDatabaseRepository: WeatherDatabaseRepository,
    private val preferences: Preferences
) : ForecastRepository {

    override suspend fun getWeatherForecast(
        latitude: Double,
        longitude: Double
    ): Result<WeatherForecastSelector> = withContext(Dispatchers.IO) {
        val result = getRemoteWeatherForecast(latitude, longitude)
        result
    }

    private suspend fun getRemoteWeatherForecast(
        latitude: Double,
        longitude: Double
    ): Result<WeatherForecastSelector> = withContext(Dispatchers.IO) {
        try {
            val response = forecastApi.getWeatherForecast(latitude.toString(), longitude.toString())

            preferences.setLastWeatherForecast(response)
            weatherDatabaseRepository.saveWeatherForecast(response, latitude, longitude)

            Result.success(WeatherForecastSelector(response))
        } catch (e: Exception) {
            Result.failure<WeatherForecastSelector>(e)
        }
    }

    override suspend fun getOfflineWeatherForecast(): Result<WeatherForecastSelector> =
        withContext(Dispatchers.IO) {
            with(preferences.getLastWeatherForecast()) {
                if (this != null) {
                    Result.success(WeatherForecastSelector(this))
                } else {
                    Result.failure(Exception())
                }
            }
        }

}
