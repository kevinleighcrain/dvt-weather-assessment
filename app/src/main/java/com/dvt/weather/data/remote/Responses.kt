package com.dvt.weather.data.remote

import com.google.gson.annotations.SerializedName

/**
 * Weather api response class
 */
data class WeatherResponse(
    @SerializedName("dt") val dateTime: Long,
    @SerializedName("id") val cityId: Long,
    @SerializedName("name") val city: String,
    val weather: ArrayList<Weather>,
    @SerializedName("main") val weatherData: WeatherData,
    @SerializedName("coord") val coordinates: CoordinatesData
)

/**
 * Weather api nested response class
 */
data class Weather(
    @SerializedName("main") val conditionName: String,
    @SerializedName("id") val id: Int
)

/**
 * Weather api nested response class
 */
data class WeatherData(
    @SerializedName("temp") val temp: String,
    @SerializedName("temp_min") val tempMin: String,
    @SerializedName("temp_max") val tempMax: String
)

/**
 * Weather api nested response class
 */
data class CoordinatesData(
    @SerializedName("lat") val latitude: Double,
    @SerializedName("lon") val longitude: Double
)

/**
 * Weather forecast api response class
 */
data class WeatherForecastResponse(
    @SerializedName("list") val weatherList: ArrayList<WeatherResponse>,
    @SerializedName("city") val cityData: CityData
)

/**
 * Weather forecast nested api response class
 */
data class CityData(
    val id: Long,
    val name: String,
    val timezone: Int
)