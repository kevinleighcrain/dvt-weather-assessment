package com.dvt.weather.data.selectors

import com.dvt.weather.data.local.Weather
import com.dvt.weather.data.remote.WeatherForecastResponse
import com.dvt.weather.data.remote.WeatherResponse
import com.google.gson.Gson

/**
 * Selector class for assisting in returning nested fields for weather entity
 *
 * @param weather Weather response
 */
data class WeatherEntitySelector(
    private val weather: Weather?
) {

    /**
     * Returns the weather forecast selector associated with weather entity
     */
    fun currentWeather() = weather?.currentWeather?.let { currentWeather ->
        Gson().fromJson(currentWeather, WeatherResponse::class.java)?.let {
            CurrentWeatherSelector(it)
        }
    }

    /**
     * Returns the weather forecast selector associated with weather entity
     */
    fun weatherForecast() = weather?.weatherForecast?.let { weatherForecast ->
        Gson().fromJson(weatherForecast, WeatherForecastResponse::class.java)?.let {
            WeatherForecastSelector(it)
        }
    }
}
