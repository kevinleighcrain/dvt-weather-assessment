package com.dvt.weather.data.local

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import com.dvt.weather.data.remote.WeatherForecastResponse
import com.dvt.weather.data.remote.WeatherResponse
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Interface for DVT Weather App preferences
 */
interface Preferences {

    /**
     * Get last current weather received by app
     *
     * @return WeatherResponse
     */
    fun getLastCurrentWeather(): WeatherResponse?

    /**
     * Set last current weather received by app
     *
     * @param response weather response
     */
    fun setLastCurrentWeather(response: WeatherResponse)

    /**
     * Get last weather forecast received by app
     *
     * @return WeatherForecastResponse
     */
    fun getLastWeatherForecast(): WeatherForecastResponse?

    /**
     * Set last weather forecast received by app
     *
     * @param response weather forecast response
     */
    fun setLastWeatherForecast(response: WeatherForecastResponse)

}

/**
 * DVT Weather App preferences implementation
 */
class PreferencesImpl @Inject constructor(
    @ApplicationContext val context: Context
) : Preferences {

    /**
     * Shared preferences object
     *
     * @see SharedPreferences
     */
    private val preferences: SharedPreferences by lazy {
        context.getSharedPreferences(
            PREFS_NAME,
            Context.MODE_PRIVATE
        )
    }

    companion object {

        /**
         * Name of preference file
         */
        const val PREFS_NAME = "com.dvt.weather"
        /**
         * Name of last current weather preference
         */
        const val PREF_LAST_CURRENT_WEATHER = "PREF_LAST_CURRENT_WEATHER"
        /**
         * Name of last weather forecast preference
         */
        const val PREF_LAST_WEATHER_FORECAST = "PREF_LAST_WEATHER_FORECAST"

    }

    override fun getLastCurrentWeather(): WeatherResponse? =
        preferences.getString(PREF_LAST_CURRENT_WEATHER, null)?.let {
            Gson().fromJson(it, WeatherResponse::class.java)
        }

    override fun setLastCurrentWeather(response: WeatherResponse) {
        val json = Gson().toJson(response, response.javaClass)
        preferences.edit()
            .putString(PREF_LAST_CURRENT_WEATHER, json)
            .apply()
    }

    override fun getLastWeatherForecast(): WeatherForecastResponse? =
        preferences.getString(PREF_LAST_WEATHER_FORECAST, null)?.let {
            Gson().fromJson(it, WeatherForecastResponse::class.java)
        }

    override fun setLastWeatherForecast(response: WeatherForecastResponse) {
        val json = Gson().toJson(response, response.javaClass)
        preferences.edit()
            .putString(PREF_LAST_WEATHER_FORECAST, json)
            .apply()
    }
}