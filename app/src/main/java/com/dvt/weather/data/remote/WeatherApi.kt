package com.dvt.weather.data.remote

import com.dvt.weather.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface for weather api requests
 *
 * See [OpenWeather current weather api](https//:openweathermap.org/current)
 */
interface WeatherApi {

    /**
     * Fetches the current weather for given location
     *
     * @param latitude See [Latitude](https://en.wikipedia.org/wiki/Latitude)
     * @param longitude See [Longitude](https://en.wikipedia.org/wiki/Longitude)
     * @param units Units temperature should be returned as i.e 'metric'
     * @param apiKey OpenWeather api key
     */
    @GET("weather")
    suspend fun getCurrentWeather(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("units") units: String = "metric",
        @Query("appid") apiKey: String = BuildConfig.OPEN_WEATHER_API_KEY
    ): WeatherResponse

}

