package com.dvt.weather.data.repository

import com.dvt.weather.data.local.WeatherDao
import com.dvt.weather.data.local.Weather
import com.dvt.weather.data.local.WeatherCurrentWeatherUpdate
import com.dvt.weather.data.local.WeatherForecastUpdate
import com.dvt.weather.data.remote.WeatherForecastResponse
import com.dvt.weather.data.remote.WeatherResponse
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Interface for weather database repository
 */
interface WeatherDatabaseRepository {

    /**
     * Saves current weather for location
     */
    suspend fun saveCurrentWeather(
        response: WeatherResponse,
        latitude: Double,
        longitude: Double
    )

    /**
     * Saves weather forecast for location
     */
    suspend fun saveWeatherForecast(
        response: WeatherForecastResponse,
        latitude: Double,
        longitude: Double
    )

}

class WeatherDatabaseRepositoryImpl @Inject constructor(
    private val weatherDao: WeatherDao
) : WeatherDatabaseRepository {

    override suspend fun saveCurrentWeather(
        response: WeatherResponse,
        latitude: Double,
        longitude: Double
    ) = withContext(Dispatchers.IO) {
        val cityId = response.cityId
        val existingWeatherLocation = weatherDao.findByCityId(cityId)
        val currentWeather = Gson().toJson(response, response.javaClass)
        if (existingWeatherLocation != null) {
            weatherDao.updateCurrentWeather(
                WeatherCurrentWeatherUpdate(existingWeatherLocation.id, currentWeather)
            )
        } else {
            val city = response.city
            val weatherLocation = Weather(
                cityId = cityId,
                city = city,
                latitude = latitude,
                longitude = longitude,
                weatherForecast = "",
                currentWeather = currentWeather
            )
            weatherDao.insert(weatherLocation)
        }
    }

    override suspend fun saveWeatherForecast(
        response: WeatherForecastResponse,
        latitude: Double,
        longitude: Double
    ) = withContext(Dispatchers.IO) {
        val cityId = response.cityData.id
        val existingWeatherLocation = weatherDao.findByCityId(cityId)
        val weatherForecastJson = Gson().toJson(response, WeatherForecastResponse::class.java)
        if (existingWeatherLocation != null) {
            weatherDao.updateWeatherForecast(
                WeatherForecastUpdate(existingWeatherLocation.id, weatherForecastJson)
            )
        } else {
            val city = response.cityData.name
            val weatherLocation = Weather(
                cityId = cityId,
                city = city,
                latitude = latitude,
                longitude = longitude,
                weatherForecast = weatherForecastJson,
                currentWeather = ""
            )
            weatherDao.insert(weatherLocation)
        }
    }

}