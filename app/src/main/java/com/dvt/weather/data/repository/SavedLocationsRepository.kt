package com.dvt.weather.data.repository

import com.dvt.weather.data.local.SavedLocation
import com.dvt.weather.data.local.SavedLocationDao
import com.dvt.weather.data.local.SavedLocationWithWeather
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Interface for saved locations repository
 */
interface SavedLocationsRepository {

    /**
     * Gets a list of saved locations
     */
    fun getAll(): Flow<List<SavedLocationWithWeather>>

    /**
     * Gets a saved location by id
     */
    suspend fun findById(id: Long): SavedLocationWithWeather?

    /**
     * Saved a new location
     */
    suspend fun saveLocation(
        cityId: Long,
        placeId: String,
        placeName: String,
        address: String,
        latitude: Double,
        longitude: Double,
        rating: Double?,
        phoneNumber: String?,
        websiteUri: String?
    )
}

class SavedLocationsRepositoryImpl @Inject constructor(
    private val saveLocationDao: SavedLocationDao
) : SavedLocationsRepository {

    override fun getAll() = saveLocationDao.getAll()

    override suspend fun findById(id: Long): SavedLocationWithWeather? = saveLocationDao.findById(id)

    override suspend fun saveLocation(
        cityId: Long,
        placeId: String,
        placeName: String,
        address: String,
        latitude: Double,
        longitude: Double,
        rating: Double?,
        phoneNumber: String?,
        websiteUri: String?
    ) = withContext(Dispatchers.IO) {
        val savedLocation = SavedLocation(
            cityId = cityId,
            placeId = placeId,
            name = placeName,
            address = address,
            latitude = latitude,
            longitude = longitude,
            rating = rating,
            phoneNumber = phoneNumber,
            websiteUri = websiteUri
        )
        saveLocationDao.insert(savedLocation)
    }

}