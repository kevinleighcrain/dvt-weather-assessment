package com.dvt.weather.data.selectors

import com.dvt.weather.data.remote.WeatherForecastResponse
import com.dvt.weather.data.remote.WeatherResponse
import java.util.*

/**
 * Selector class for assisting in returning nested fields for weather forecast response
 *
 * @param response Weather forecast response
 */
data class WeatherForecastSelector(
    private val response: WeatherForecastResponse
) {

    /**
     * Returns weather response for provided day and hour of the day
     *
     * If no weather response is found for hour of day, then method will default to the first
     * weather response found for provided day.
     *
     * @param day The day weather response is for
     * @param hourOfDay The hour of day weather response is for
     */
    fun forDayAndHour(day: Int, hourOfDay: Int): WeatherResponse? {
        val timezone = with(TimeZone.getDefault()) {
            rawOffset = response.cityData.timezone
            this
        }

        val weatherListForDay = response.weatherList.filter {
            val calendar = Calendar.getInstance(timezone)
            calendar.time = Date(it.dateTime * 1000)
            calendar.get(Calendar.DATE) == day
        }

        var weatherForDay = weatherListForDay.firstOrNull {
            val calendar = Calendar.getInstance(timezone)
            calendar.time = Date(it.dateTime * 1000)
            val weatherHour = calendar.get(Calendar.HOUR_OF_DAY)
            hourOfDay >= weatherHour && hourOfDay < weatherHour + 3
        }

        if (weatherForDay == null) {
            weatherForDay = weatherListForDay.firstOrNull()
        }

        if (weatherForDay != null) {
            return weatherForDay
        }

        return null
    }

}