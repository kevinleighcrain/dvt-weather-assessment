package com.dvt.weather.data.local

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Insert
import androidx.room.Update
import androidx.room.Transaction
import androidx.room.OnConflictStrategy
import androidx.room.Database
import androidx.room.RoomDatabase
import kotlinx.coroutines.flow.Flow

/**
 * Dao for weather entity CRUD operations
 *
 * @see Weather
 */
@Dao
interface WeatherDao {

    /**
     * Fetches a list of weather entities
     */
    @Query("SELECT * from weather ORDER BY city ASC")
    fun getAll(): Flow<List<Weather>>

    /**
     * Fetches a weather entity for a given city
     *
     * @param cityId City to return weather for
     */
    @Query("SELECT * from weather WHERE cityId = :cityId")
    suspend fun findByCityId(cityId: Long): Weather?

    /**
     * Updates current weather for weather entity
     *
     * @param update Partial db class for updating current weather field
     */
    @Update(entity = Weather::class)
    fun updateCurrentWeather(update: WeatherCurrentWeatherUpdate)

    /**
     * Updates weather forecast for weather entity
     *
     * @param update Partial db class for updating weather forecast field
     */
    @Update(entity = Weather::class)
    fun updateWeatherForecast(update: WeatherForecastUpdate)

    /**
     * Inserts new weather entity
     *
     * @param weather Weather entity to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(weather: Weather)

}

/**
 * Dao for saved locations entity CRUD operations
 *
 * @see SavedLocation
 */
@Dao
interface SavedLocationDao {

    /**
     * Fetches a list of saved locations entities
     */
    @Transaction
    @Query("SELECT * from savedlocation ORDER BY name ASC")
    fun getAll(): Flow<List<SavedLocationWithWeather>>

    /**
     * Fetches a of saved location entity by id
     */
    @Transaction
    @Query("SELECT * from savedlocation WHERE id = :id ORDER BY name ASC")
    suspend fun findById(id: Long): SavedLocationWithWeather?

    /**
     * Inserts new saved location entity
     *
     * @param savedLocation Saved location entity to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(savedLocation: SavedLocation)

}

/**
 * Room database for creating weather dao implementations
 */
@Database(
    entities = [
        Weather::class,
        SavedLocation::class
    ],
    version = 1,
    exportSchema = false
)
abstract class WeatherDatabase : RoomDatabase() {

    /**
     * Returns weather dao implementation
     */
    abstract fun weatherDao(): WeatherDao

    /**
     * Returns saved location dao implementation
     */
    abstract fun savedLocationDao(): SavedLocationDao

}

