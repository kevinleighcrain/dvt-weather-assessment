package com.dvt.weather.data.local

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import androidx.room.Index
import androidx.room.Embedded
import androidx.room.Relation

/**
 * Weather DB entity
 */
@Entity(indices = [Index(value = ["cityId", "city"], unique = true)])
data class Weather(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    @ColumnInfo val city: String,
    @ColumnInfo val cityId: Long,
    @ColumnInfo val latitude: Double,
    @ColumnInfo val longitude: Double,
    @ColumnInfo val currentWeather: String? = null,
    @ColumnInfo val weatherForecast: String? = null
)

/**
 * Partial DB entity to update current weather
 */
@Entity
data class WeatherCurrentWeatherUpdate(
    @ColumnInfo val id: Long,
    @ColumnInfo val currentWeather: String
)

/**
 * Partial DB entity to update weather forecast
 */
@Entity
data class WeatherForecastUpdate(
    @ColumnInfo val id: Long,
    @ColumnInfo val weatherForecast: String
)

/**
 * Saved location DB entity
 */
@Entity(indices = [Index(value = ["placeId"], unique = true)])
data class SavedLocation(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    @ColumnInfo val placeId: String,
    @ColumnInfo val cityId: Long,
    @ColumnInfo val name: String,
    @ColumnInfo val address: String?,
    @ColumnInfo val latitude: Double,
    @ColumnInfo val longitude: Double,
    @ColumnInfo val rating: Double?,
    @ColumnInfo val phoneNumber: String?,
    @ColumnInfo val websiteUri: String?
)

/**
 * Join class for saved location and weather entity classes
 */
data class SavedLocationWithWeather(
    @Embedded val savedLocation: SavedLocation,
    @Relation(
        parentColumn = "cityId",
        entityColumn = "cityId"
    )
    val weather: Weather
)
