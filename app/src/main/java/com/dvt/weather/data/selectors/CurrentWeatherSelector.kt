package com.dvt.weather.data.selectors

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IntDef
import com.dvt.weather.R
import com.dvt.weather.data.remote.WeatherResponse
import java.util.*
import kotlin.math.roundToInt

/**
 * Selector class for assisting in returning nested fields for weather response
 *
 * @param response Weather response
 * @param dayOfWeek Day of week for weather response
 */
data class CurrentWeatherSelector(
    private val response: WeatherResponse?,
    private val dayOfWeek: String? = null
) {

    /**
     * Returns the city id associated with weather response
     */
    fun cityId() = response?.cityId

    /**
     * Returns the weather condition name associated with weather response
     */
    fun conditionName() = response?.weather?.firstOrNull()?.conditionName

    /**
     * Returns the day of week associated with weather response
     */
    fun dayOfWeek(): String? = dayOfWeek
        ?: with(Calendar.getInstance()) {
            time = Date((response?.dateTime ?: 0) * 1000)
            getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ROOT)
        }

    /**
     * Returns the current temperature associated with weather response
     */
    fun temp() = response?.weatherData?.temp?.toDoubleOrNull()?.roundToInt()?.toString() ?: "--"

    /**
     * Returns the min temperature associated with weather response
     */
    fun tempMin() =
        response?.weatherData?.tempMin?.toDoubleOrNull()?.roundToInt()?.toString() ?: "--"

    /**
     * Returns the max temperature associated with weather response
     */
    fun tempMax() =
        response?.weatherData?.tempMax?.toDoubleOrNull()?.roundToInt()?.toString() ?: "--"

    /**
     * Returns weather background associated with weather response
     */
    @DrawableRes
    fun backgroundImage() = when (weatherConditionType()) {
        WeatherConditionType.CLOUDY -> R.drawable.forest_cloudy
        WeatherConditionType.SNOWY -> R.drawable.forest_cloudy // No background for snowy, default to cloudy
        WeatherConditionType.RAINY -> R.drawable.forest_rainy
        else -> R.drawable.forest_sunny
    }

    /**
     * Returns the weather background color associated with weather response
     */
    @ColorRes
    fun backgroundColor() = when (weatherConditionType()) {
        WeatherConditionType.CLOUDY -> R.color.cloudy
        WeatherConditionType.SNOWY -> R.color.cloudy // No color for snowy, default to cloudy
        WeatherConditionType.RAINY -> R.color.rainy
        else -> R.color.sunny
    }

    /**
     * Returns the weather icon associated with weather response
     */
    @DrawableRes
    fun icon() = when (weatherConditionType()) {
        WeatherConditionType.CLOUDY -> R.drawable.ic_cloudy
        WeatherConditionType.SNOWY -> R.drawable.ic_cloudy // No icon for snowy, default to cloudy
        WeatherConditionType.RAINY -> R.drawable.ic_rainy
        else -> R.drawable.ic_sunny
    }

    /**
     * Returns the weather condition type associated with weather response
     *
     * See [OpenWeather weather condition codes](https://openweathermap.org/weather-conditions)
     */
    @WeatherConditionType
    fun weatherConditionType() =
        when (response?.weather?.firstOrNull()?.id ?: 0) {
            in 200..232 -> WeatherConditionType.RAINY
            in 300..321 -> WeatherConditionType.RAINY
            in 500..532 -> WeatherConditionType.RAINY
            in 600..622 -> WeatherConditionType.SNOWY
            in 801..804 -> WeatherConditionType.CLOUDY
            else -> WeatherConditionType.SUNNY
        }

}

/**
 * Weather conditions definition class supported by app
 */
@IntDef(
    WeatherConditionType.SUNNY,
    WeatherConditionType.CLOUDY,
    WeatherConditionType.RAINY,
    WeatherConditionType.SNOWY
)
annotation class WeatherConditionType {
    companion object {
        const val SUNNY = 0
        const val CLOUDY = 1
        const val RAINY = 2
        const val SNOWY = 3
    }
}