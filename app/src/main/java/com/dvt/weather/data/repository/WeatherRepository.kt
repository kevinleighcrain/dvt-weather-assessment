package com.dvt.weather.data.repository

import android.content.Context
import com.dvt.weather.data.local.Preferences
import com.dvt.weather.data.remote.WeatherApi
import com.dvt.weather.data.selectors.CurrentWeatherSelector
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Interface for weather repository
 */
interface WeatherRepository {

    /**
     * Gets the current weather from api
     */
    suspend fun getCurrentWeather(
        latitude: Double,
        longitude: Double,
        isCurrentLocation: Boolean
    ): Result<CurrentWeatherSelector>

    /**
     * Gets current weather last received by app when network is offline
     */
    suspend fun getOfflineCurrentWeather(): Result<CurrentWeatherSelector>

}

class WeatherRepositoryImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val weatherApi: WeatherApi,
    private val weatherDatabaseRepository: WeatherDatabaseRepository,
    private val preferences: Preferences
) : WeatherRepository {

    override suspend fun getCurrentWeather(
        latitude: Double,
        longitude: Double,
        isCurrentLocation: Boolean
    ): Result<CurrentWeatherSelector> = withContext(Dispatchers.IO) {
        getRemoteCurrentWeather(latitude, longitude, isCurrentLocation)
    }

    override suspend fun getOfflineCurrentWeather(): Result<CurrentWeatherSelector> =
        getLocalCurrentWeather()

    private suspend fun getRemoteCurrentWeather(
        latitude: Double,
        longitude: Double,
        isCurrentLocation: Boolean
    ): Result<CurrentWeatherSelector> = withContext(Dispatchers.IO) {
        try {
            val response = weatherApi.getCurrentWeather(latitude.toString(), longitude.toString())

            if (isCurrentLocation) {
                preferences.setLastCurrentWeather(response)
            }

            weatherDatabaseRepository.saveCurrentWeather(response, latitude, longitude)

            Result.success(CurrentWeatherSelector(response))
        } catch (e: Exception) {
            Result.failure<CurrentWeatherSelector>(e)
        }
    }

    private suspend fun getLocalCurrentWeather(): Result<CurrentWeatherSelector> =
        withContext(Dispatchers.IO) {
            with(preferences.getLastCurrentWeather()) {
                if (this != null) {
                    Result.success(CurrentWeatherSelector(this))
                } else {
                    Result.failure(Exception())
                }
            }
        }

}
