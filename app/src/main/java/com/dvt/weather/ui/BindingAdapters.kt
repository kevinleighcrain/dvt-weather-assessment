package com.dvt.weather.ui

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.navigation.Navigation
import com.dvt.weather.R
import com.dvt.weather.data.selectors.CurrentWeatherSelector

/**
 * Binds weather background to view
 *
 * @param currentWeather The current weather
 */
@BindingAdapter("backgroundImage")
fun View.backgroundImage(backgroundImage: Int?) =
    setBackgroundResource(backgroundImage ?: R.drawable.forest_sunny)

/**
 * Binds weather background color to view
 *
 * @param currentWeather The current weather
 */
@BindingAdapter("backgroundColor")
fun View.backgroundColor(currentWeather: CurrentWeatherSelector?) {
    val color = currentWeather?.backgroundColor() ?: R.color.sunny
    setBackgroundColor(ContextCompat.getColor(context, color))
}

/**
 * Binds weather icon to view
 *
 * @param currentWeather The current weather
 */
@BindingAdapter("icon")
fun AppCompatImageView.setIconResource(currentWeather: CurrentWeatherSelector?) {
    currentWeather?.run {
        setImageResource(currentWeather.icon())
    }
}

/**
 * Binds background visibility to view
 *
 * @param gone Whether view should be gone
 */
@BindingAdapter("gone")
fun View.setGone(gone: Boolean) {
    visibility = if (gone) View.GONE else View.VISIBLE
}

/**
 * Binds navigation ui destination click listener to view
 *
 * @param destination The destination that view should navigate to when clicked
 */
@BindingAdapter("destination")
fun View.setDestination(destination: Int) {
    setOnClickListener(Navigation.createNavigateOnClickListener(destination))
}
