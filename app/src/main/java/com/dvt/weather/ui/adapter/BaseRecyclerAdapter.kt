package com.dvt.weather.ui.adapter

import androidx.recyclerview.widget.RecyclerView

/**
 * Base class for recycler adapters
 *
 * @see RecyclerView.Adapter
 */
abstract class BaseRecyclerAdapter<VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int) = position.toLong()

}