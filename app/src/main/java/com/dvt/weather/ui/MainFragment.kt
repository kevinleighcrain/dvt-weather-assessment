package com.dvt.weather.ui

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.dvt.weather.R
import com.dvt.weather.data.repository.ForecastRepository
import com.dvt.weather.data.repository.WeatherRepository
import com.dvt.weather.data.selectors.CurrentWeatherSelector
import com.dvt.weather.data.selectors.WeatherForecastSelector
import com.dvt.weather.databinding.FragmentMainBinding
import com.dvt.weather.helpers.isNetworkAvailable
import com.dvt.weather.manager.DeviceLocationManager
import com.dvt.weather.manager.DeviceLocationManager.Companion.ERROR_PERMISSION_DENIED
import com.dvt.weather.manager.DeviceLocationManager.Companion.ERROR_PERMISSION_PERMANENTLY_DENIED
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Main fragment for displaying current weather and weather forecast
 */
@AndroidEntryPoint
class MainFragment : Fragment() {

    private val mainViewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel.fragment = this
        lifecycle.addObserver(mainViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(FragmentMainBinding.inflate(inflater, container, false)) {
        viewModel = mainViewModel

        val navController = findNavController()
        mainNav.setupWithNavController(navController)

        root
    }

}

class MainViewModel @ViewModelInject
constructor(
    @ApplicationContext private val context: Context,
    private val deviceLocationManager: DeviceLocationManager,
    private val weatherRepository: WeatherRepository,
    private val forecastRepository: ForecastRepository
) : ViewModel(), LifecycleObserver {

    lateinit var fragment: Fragment

    val error = ObservableField<String>()
    val errorCode = ObservableField<Int>()
    val loading = ObservableField<Boolean>()
    val currentWeather = ObservableField<CurrentWeatherSelector>()
    val weatherForecast = ObservableField<WeatherForecastSelector>()

    private val onLocation = Observer<Location> { location ->
        Log.d(javaClass.simpleName, "Load data...")

        GlobalScope.launch {
            error.set(null)
            loading.set(true)

            loadCurrentWeather(location)
            loadWeatherForecast(location)

            loading.set(null)
        }
    }

    private val onLocationError = Observer<Int> { errorCode ->
        loading.set(false)
        this.errorCode.set(errorCode)
        when (errorCode) {
            ERROR_PERMISSION_DENIED -> {
                init()
            }
            ERROR_PERMISSION_PERMANENTLY_DENIED -> {
                error.set(fragment.getString(R.string.error_location_permission_denied))
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        init()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        dispose()
    }

    fun onMenuIconClick(view: View) {
        fragment.view?.run {
            val binding = DataBindingUtil.getBinding<FragmentMainBinding>(this)
            binding?.drawer?.openDrawer(GravityCompat.START)
        }
    }

    fun onTapToRetry(view: View) {
        dispose()
        init()
    }

    private fun init() {
        if (context.isNetworkAvailable()) {
            deviceLocationManager.locationLiveData.observe(fragment, onLocation)
            deviceLocationManager.locationErrorLiveData.observe(fragment, onLocationError)

            loading.set(true)
            deviceLocationManager.fetch()
        } else {
            Log.d(javaClass.simpleName, "Load offline data...")

            loadOfflineCurrentWeather()
            loadOfflineWeatherForecast()
        }
    }

    private fun dispose() {
        deviceLocationManager.locationLiveData.removeObservers(fragment)
        deviceLocationManager.locationErrorLiveData.removeObservers(fragment)
    }

    private suspend fun loadCurrentWeather(location: Location) {
        Log.d(javaClass.simpleName, "Load current weather data...")

        val result = weatherRepository.getCurrentWeather(
            latitude = location.latitude,
            longitude = location.longitude,
            isCurrentLocation = true
        )
        currentWeather.set(result.getOrNull())
    }

    private suspend fun loadWeatherForecast(location: Location) {
        Log.d(javaClass.simpleName, "Load weather forecast data...")

        val result = forecastRepository.getWeatherForecast(
            latitude = location.latitude,
            longitude = location.longitude
        )
        weatherForecast.set(result.getOrNull())
    }

    private fun loadOfflineCurrentWeather() {
        Log.d(javaClass.simpleName, "Load offline current weather data...")

        GlobalScope.launch {
            val result = weatherRepository.getOfflineCurrentWeather()
            currentWeather.set(result.getOrNull())
        }
    }

    private fun loadOfflineWeatherForecast() {
        Log.d(javaClass.simpleName, "Load offline weather forecast data...")

        GlobalScope.launch {
            val result = forecastRepository.getOfflineWeatherForecast()
            weatherForecast.set(result.getOrNull())
        }
    }


}
