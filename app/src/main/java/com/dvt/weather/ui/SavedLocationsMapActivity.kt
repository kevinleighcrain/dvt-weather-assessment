package com.dvt.weather.ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import androidx.activity.viewModels
import androidx.core.util.Consumer
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.findFragment
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.dvt.weather.R
import com.dvt.weather.data.local.Preferences
import com.dvt.weather.data.local.SavedLocationWithWeather
import com.dvt.weather.data.remote.WeatherResponse
import com.dvt.weather.data.repository.SavedLocationsRepository
import com.dvt.weather.databinding.ActivitySavedLocationsMapBinding
import com.dvt.weather.ui.adapter.MapLocation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Activity for displaying saved locations on Google Map
 *
 * @see GoogleMap
 *
 * See [Maps SDK for Android](https://developers.google.com/maps/documentation/android-sdk/start)
 */
@AndroidEntryPoint
class SavedLocationsMapActivity : BaseActivity() {

    private val viewModel by viewModels<SavedLocationsMapViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySavedLocationsMapBinding>(
            this,
            R.layout.activity_saved_locations_map
        )
        binding.viewModel = viewModel
        val mapFragment =
            findViewById<FrameLayout>(R.id.map).findFragment() as SupportMapFragment?
        mapFragment?.getMapAsync { viewModel.handleOnMapReady(this, it) }
        viewModel.initBottomSheetBehavior(binding)
    }

}

class SavedLocationsMapViewModel @ViewModelInject constructor(
    @ApplicationContext private val context: Context,
    private val savedLocationsRepository: SavedLocationsRepository,
    private val preferences: Preferences
) : ViewModel() {

    lateinit var map: GoogleMap
    lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    var bottomSheetTitle = ObservableField<String>(getBottomSheetTitle(STATE_COLLAPSED))
    val mapLocations = ObservableField<List<MapLocation>>()
    val mapLocationsClickConsumer = Consumer<MapLocation> {
        onSavedLocationClick(it)
    }

    fun handleOnMapReady(activity: Activity, map: GoogleMap) {
        this.map = map
        GlobalScope.launch {
            savedLocationsRepository.getAll().collect {
                handleOnSaveLocations(activity, it)
            }
        }
    }

    private fun handleOnSaveLocations(
        activity: Activity,
        savedLocations: List<SavedLocationWithWeather>
    ) {
        val mapLocations = mutableListOf<MapLocation>()
        preferences.getLastCurrentWeather()?.let {
            with(it) {
                val name = "Current(${city}) - ${weatherData.temp}C"
                val currentLocation = MapLocation(name, coordinates.latitude, coordinates.longitude)
                mapLocations.add(currentLocation)

                activity.runOnUiThread {
                    addMapLocationMarker(currentLocation)
                    zoomToMapLocation(currentLocation)
                }
            }
        }

        savedLocations.forEach {
            with(it.savedLocation) {
                val currentWeather =
                    Gson().fromJson(it.weather.currentWeather, WeatherResponse::class.java)
                val mapLocation = MapLocation(
                    "$name - ${currentWeather.weatherData.temp}C",
                    latitude,
                    longitude
                )
                mapLocations.add(mapLocation)

                activity.runOnUiThread {
                    addMapLocationMarker(mapLocation)
                }
            }
        }
        this.mapLocations.set(mapLocations)
    }

    private fun addMapLocationMarker(mapLocation: MapLocation) {
        with(mapLocation) {
            val marker = MarkerOptions()
                .position(LatLng(latitude, longitude))
                .title(name)
            map.addMarker(marker)
        }
    }

    private fun getBottomSheetTitle(state: Int) = when (state) {
        STATE_COLLAPSED -> context.getString(R.string.title_saved_locations_bottom_sheet_collapsed)
        STATE_EXPANDED -> context.getString(R.string.title_saved_locations_bottom_sheet_expanded)
        else -> ""
    }

    fun initBottomSheetBehavior(binding: ActivitySavedLocationsMapBinding) {
        binding.root.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                binding.root.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val height: Int = binding.bottomSheetPeekView.measuredHeight
                bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
                bottomSheetBehavior.state = STATE_COLLAPSED
                bottomSheetBehavior.setPeekHeight(height, true)
                bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
            }
        })
    }

    private val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(view: View, newState: Int) {
            bottomSheetTitle.set(getBottomSheetTitle(newState))
        }

        override fun onSlide(view: View, slideOffset: Float) = Unit
    }

    fun onViewSavedLocationsClick(view: View) {
        if (bottomSheetBehavior.state == STATE_COLLAPSED) bottomSheetBehavior.state = STATE_EXPANDED
        else if (bottomSheetBehavior.state == STATE_EXPANDED) bottomSheetBehavior.state =
            STATE_COLLAPSED
    }

    private fun onSavedLocationClick(mapLocation: MapLocation) {
        bottomSheetBehavior.state = STATE_COLLAPSED

        zoomToMapLocation(mapLocation)
    }

    private fun zoomToMapLocation(mapLocation: MapLocation) {
        with(mapLocation) {
            val latLng = LatLng(latitude, longitude)
            val zoomAmount = 15f
            val zoomCameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoomAmount)
            map.moveCamera(zoomCameraUpdate)
            map.animateCamera(CameraUpdateFactory.zoomIn())
            map.animateCamera(CameraUpdateFactory.zoomTo(15f), 2000, null)
        }
    }

}