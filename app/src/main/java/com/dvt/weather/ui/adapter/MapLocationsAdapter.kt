package com.dvt.weather.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dvt.weather.BR
import com.dvt.weather.databinding.ItemMapLocationBinding

/**
 * Adapter class for displaying locations for map
 *
 * @param items List of map locations
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
class MapLocationsAdapter(
    private val items: List<MapLocation>,
    private val onItemClickConsumer: Consumer<MapLocation>?
) : BaseRecyclerAdapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMapLocationBinding.inflate(inflater, parent, false)
        return object : RecyclerView.ViewHolder(binding.root) {}
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<ItemMapLocationBinding>(holder.itemView)
        binding?.setVariable(BR.viewModel, MapLocationItemViewModel(items[position]))
        binding?.root?.setOnClickListener {
            onItemClickConsumer?.accept(items[position])
        }
    }
}

/**
 * View modal used for binding map location item to layout
 *
 * @param item Saved location
 */
data class MapLocationItemViewModel(val item: MapLocation)

/**
 * Data class for map location
 */
data class MapLocation(val name: String, val latitude: Double, val longitude: Double)

/**
 * Binding adapter used to bind map locations adapter to recycler view
 *
 * @param items List of map locations
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
@BindingAdapter("items", "onItemClickConsumer", requireAll = true)
fun RecyclerView.setAdapter(
    items: List<MapLocation>?,
    onItemClickConsumer: Consumer<MapLocation>?
) {
    items?.run {
        adapter = MapLocationsAdapter(items, onItemClickConsumer)
    }
}
