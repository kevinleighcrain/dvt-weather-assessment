package com.dvt.weather.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dvt.weather.BR
import com.dvt.weather.data.local.SavedLocation
import com.dvt.weather.databinding.ItemSavedLocationFieldBinding

/**
 * Adapter class for displaying saved location fields
 *
 * @param items List of saved location fields
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
class SavedLocationFieldsAdapter(
    private val items: List<SavedLocationField>,
    private val onItemClickConsumer: Consumer<SavedLocationField>?
) : BaseRecyclerAdapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemSavedLocationFieldBinding.inflate(inflater, parent, false)
        return object : RecyclerView.ViewHolder(binding.root) {}
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<ItemSavedLocationFieldBinding>(holder.itemView)
        binding?.setVariable(BR.viewModel, SavedLocationFieldViewModel(items[position]))
        binding?.root?.setOnClickListener {
            onItemClickConsumer?.accept(items[position])
        }
    }
}

/**
 * View modal used for binding saved location fields item to layout
 *
 * @param item Saved location field
 */
data class SavedLocationFieldViewModel(val item: SavedLocationField)

data class SavedLocationField(
    val label: String,
    val value: String
)

/**
 * Binding adapter used to bind saved locations adapter to recycler view
 *
 * @param items List of saved locations
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
@BindingAdapter("savedLocation", "onItemClickConsumer", requireAll = false)
fun RecyclerView.setAdapter(
    savedLocation: SavedLocation?,
    onItemClickConsumer: Consumer<SavedLocationField>?
) {
    savedLocation?.run {
        val items = mutableListOf<SavedLocationField>()
        address?.run {
            items.add(SavedLocationField("Address:", address))
        }
        phoneNumber?.run {
            items.add(SavedLocationField("Phone number:", phoneNumber))
        }
        websiteUri?.run {
            items.add(SavedLocationField("Website:", websiteUri))
        }
        rating?.run {
            items.add(SavedLocationField("Rating:", rating.toString()))
        }
        adapter = SavedLocationFieldsAdapter(items, onItemClickConsumer)
    }
}