package com.dvt.weather.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dvt.weather.BR
import com.dvt.weather.data.local.SavedLocationWithWeather
import com.dvt.weather.databinding.ItemSavedLocationBinding

/**
 * Adapter class for displaying saved locations
 *
 * @param items List of saved locations
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
class SavedLocationsAdapter(
    private val items: List<SavedLocationWithWeather>,
    private val onItemClickConsumer: Consumer<SavedLocationWithWeather>?
) : BaseRecyclerAdapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemSavedLocationBinding.inflate(inflater, parent, false)
        return object : RecyclerView.ViewHolder(binding.root) {}
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<ItemSavedLocationBinding>(holder.itemView)
        binding?.setVariable(BR.viewModel, SavedLocationItemViewModel(items[position]))
        binding?.root?.setOnClickListener {
            onItemClickConsumer?.accept(items[position])
        }
    }
}

/**
 * View modal used for binding saved location item to layout
 *
 * @param item Saved location
 */
data class SavedLocationItemViewModel(val item: SavedLocationWithWeather)

/**
 * Binding adapter used to bind saved locations adapter to recycler view
 *
 * @param items List of saved locations
 * @param onItemClickConsumer Consumer object which is invoked when item is clicked
 */
@BindingAdapter("items", "onItemClickConsumer", requireAll = true)
fun RecyclerView.setAdapter(
    items: List<SavedLocationWithWeather>?,
    onItemClickConsumer: Consumer<SavedLocationWithWeather>?
) {
    items?.run {
        adapter = SavedLocationsAdapter(items, onItemClickConsumer)
    }
}