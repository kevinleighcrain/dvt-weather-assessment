package com.dvt.weather.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import com.dvt.weather.R
import com.dvt.weather.data.local.SavedLocationWithWeather
import com.dvt.weather.data.repository.SavedLocationsRepository
import com.dvt.weather.data.repository.WeatherRepository
import com.dvt.weather.databinding.FragmentSavedLocationsBinding
import com.dvt.weather.helpers.PlacesAutoCompleteHelper
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.model.Place
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Fragment for displaying saved locations
 */
@AndroidEntryPoint
class SavedLocationsFragment : Fragment() {

    private val savedLocationsViewModel by viewModels<SavedLocationsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(FragmentSavedLocationsBinding.inflate(inflater, container, false)) {
        savedLocationsViewModel.fragment = this@SavedLocationsFragment
        viewModel = savedLocationsViewModel
        root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        savedLocationsViewModel.onActivityResult(requestCode, resultCode, data)
    }

}

class SavedLocationsViewModel @ViewModelInject constructor(
    private val weatherRepository: WeatherRepository,
    private val savedLocationsRepository: SavedLocationsRepository
) : ViewModel() {

    lateinit var fragment: Fragment

    val loading = ObservableField<Boolean>()
    val savedLocations = ObservableField<List<SavedLocationWithWeather>>()
    val savedLocationClickConsumer = Consumer<SavedLocationWithWeather> { savedLocation ->
        val navController = Navigation.findNavController(
            fragment.requireActivity(),
            R.id.nav_host_fragment
        )
        val args = SavedLocationDetailsFragmentArgs(
            saveLocationId = savedLocation.savedLocation.id
        )
        navController.navigate(
            R.id.action_savedLocations_to_savedLocationDetailsFragment,
            args.toBundle()
        )
    }

    init {
        GlobalScope.launch {
            savedLocationsRepository.getAll().collect {
                savedLocations.set(it)
            }
        }
    }

    fun onAddWeatherLocationClick(view: View) {
        PlacesAutoCompleteHelper.showAutoComplete(fragment)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        PlacesAutoCompleteHelper.onActivityResult(
            requestCode, resultCode, data,
            onPlace = this::onPlace,
            onPlaceError = this::onPlaceError
        )
    }

    private fun onPlace(place: Place) {
        GlobalScope.launch {
            val latitude = place.latLng?.latitude ?: 0.0
            val longitude = place.latLng?.longitude ?: 0.0
            val result = weatherRepository.getCurrentWeather(latitude, longitude, false)
            val currentWeather = result.getOrNull()
            if (currentWeather != null) {
                val cityId = currentWeather.cityId() ?: 0
                saveLocation(cityId, place, latitude, longitude)
            }
        }
    }

    private suspend fun saveLocation(
        cityId: Long,
        place: Place,
        latitude: Double,
        longitude: Double
    ) {
        loading.set(true)
        delay(1000)
        savedLocationsRepository.saveLocation(
            cityId = cityId,
            placeId = place.id ?: "",
            placeName = place.name ?: "",
            address = place.address ?: "",
            rating = place.rating,
            phoneNumber = place.phoneNumber,
            websiteUri = place.websiteUri?.toString(),
            latitude = latitude,
            longitude = longitude
        )
        loading.set(false)
    }

    private fun onPlaceError(status: Status) {
        Log.d(javaClass.simpleName, "Error receiving place: ${status.statusMessage}")
    }
}
