package com.dvt.weather.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dvt.weather.BR
import com.dvt.weather.data.selectors.CurrentWeatherSelector
import com.dvt.weather.data.selectors.WeatherForecastSelector
import com.dvt.weather.databinding.ItemForecastBinding
import java.util.*

/**
 * Adapter class for displaying weather forecast
 *
 * @param items List of weather forecast selectors
 */
class WeatherForecastAdapter(
    val items: List<CurrentWeatherSelector>
) : BaseRecyclerAdapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            ItemForecastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return object : RecyclerView.ViewHolder(binding.root) {}
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<ItemForecastBinding>(holder.itemView)
        binding?.setVariable(BR.viewModel, WeatherForecastItemViewModel(items[position]))
    }
}

/**
 * View modal used for binding weather forecast item to layout
 *
 * @param item Weather forecast
 */
data class WeatherForecastItemViewModel(val item: CurrentWeatherSelector)

/**
 * Binding adapter used to bind weather forecast adapter to recycler view
 *
 * @param weatherForecast Weather forecast selector
 */
@BindingAdapter("weatherForecast")
fun RecyclerView.setAdapter(weatherForecast: WeatherForecastSelector?) {
    val calendar = Calendar.getInstance()
    val items = mutableListOf<CurrentWeatherSelector>()
    for (day in 0 until 5) {
        calendar.add(Calendar.DATE, 1)
        val dayOfWeek = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ROOT)
        if (weatherForecast != null) {
            val dayOfMonth = calendar.get(Calendar.DATE)
            weatherForecast.forDayAndHour(dayOfMonth, calendar.get(Calendar.HOUR))?.let {
                items.add(CurrentWeatherSelector(it, dayOfWeek))
            }
        } else {
            items.add(CurrentWeatherSelector(null, dayOfWeek))
        }
    }
    adapter = WeatherForecastAdapter(items)
}
