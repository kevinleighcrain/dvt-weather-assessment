package com.dvt.weather.ui

import android.os.Bundle
import com.dvt.weather.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity for DVT Weather App
 */
@AndroidEntryPoint
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}