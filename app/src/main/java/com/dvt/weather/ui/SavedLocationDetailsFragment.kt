package com.dvt.weather.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.navArgs
import com.dvt.weather.data.local.SavedLocation
import com.dvt.weather.data.repository.ForecastRepository
import com.dvt.weather.data.repository.SavedLocationsRepository
import com.dvt.weather.data.selectors.WeatherEntitySelector
import com.dvt.weather.data.selectors.WeatherForecastSelector
import com.dvt.weather.databinding.FragmentSavedLocationDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Fragment for displaying saved location details
 */
@AndroidEntryPoint
class SavedLocationDetailsFragment : Fragment() {

    private val args = navArgs<SavedLocationDetailsFragmentArgs>()
    private val savedLocationsDetailsViewModel by viewModels<SavedLocationDetailsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedLocationsDetailsViewModel.savedLocationId = args.value.saveLocationId
        lifecycle.addObserver(savedLocationsDetailsViewModel)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(savedLocationsDetailsViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(FragmentSavedLocationDetailsBinding.inflate(inflater, container, false)) {
        viewModel = savedLocationsDetailsViewModel
        root
    }

}

class SavedLocationDetailsViewModel @ViewModelInject constructor(
    private val savedLocationsRepository: SavedLocationsRepository,
    private val forecastRepository: ForecastRepository
) : ViewModel(), LifecycleObserver {

    var savedLocationId: Long = 0

    val loading = ObservableField<Boolean>()
    val savedLocation = ObservableField<SavedLocation>()
    val weather = ObservableField<WeatherEntitySelector>()
    val weatherForecast = ObservableField<WeatherForecastSelector>()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        GlobalScope.launch {
            getSavedLocation()
        }
    }

    private suspend fun getSavedLocation() {
        savedLocationsRepository.findById(savedLocationId)?.let { entity ->
            savedLocation.set(entity.savedLocation)
            weather.set(WeatherEntitySelector(entity.weather))
            weatherForecast.set(WeatherEntitySelector(entity.weather).weatherForecast())

            fetchWeatherForecast(entity.savedLocation)
        }
    }

    private suspend fun fetchWeatherForecast(savedLocation: SavedLocation) {
        with(savedLocation) {
            loading.set(true)
            forecastRepository.getWeatherForecast(latitude, longitude).getOrNull()?.let {
                weatherForecast.set(it)
            }
            loading.set(false)
        }
    }
}