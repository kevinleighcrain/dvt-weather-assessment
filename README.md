# DVT Weather App

Assessment Weather app for DVT application using Android framework

### Contents
- [Unit testing](#unit-testing)
- [Static analyzer / Code style](#static-analyzer)
- [Deploy app with CI/CD](#ci-cd)

<a name="unit-testing"/>

# Unit Testing

To run test suite you can use gradle command below:
```
./gradlew test
```
> Note: please run command from root of project

<a name="static-analyzer"/>

# Static analyzer

To run static analyzer / code style, you can use gradle command below:
```
./gradlew detekt
```

> Note: please run command from root of project

<a name="ci-cd"/>

# CI/CD

To run CI/CD build please navigate to AppCenter url [here](https://appcenter.ms/users/kevin.c-1/apps/DVT-Weather-App/build/branches).
Then follow steps to create and run a new build using AppCenter.